#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
Created on Sat Apr 29 16:57:48 2017

@author: wang bei bei
"""
import queue,socketserver,re,time,requests
from spider_ip_and_port.TCP_sender import TCP_sender
from spider_ip_and_port.check_proxy import check_proxy
from spider_ip_and_port.receiving_ip_and_port import receiving_ip_and_port

#切换代理
from webdriver_change_proxy import webdriver_change_proxy
#去重的函数
from de_weight.de_weight import de_weight
#创建driver
from create_driver import Driver
#拓展用函数
from receiver import receiver,TCP_receiver
#切换UA用的库
from fake_useragent import UserAgent
#bs处理超链接
from bs4 import BeautifulSoup
#引进线程锁 
thread_num=1
from lock import semaphore
import threading,time
lock=threading.Lock()
lock=semaphore(lock,thread_num)

spider_ip='127.0.0.1'
remoted_ip='127.0.0.1'


#待装填队列
pr=queue.Queue()  #存放id
po=queue.Queue()  #存放port
#########杂七杂八的端口##############
ip_sending_port=8880
ip_receiving_port=8870
url_checking_receiving_port=9990
url_checking_sending_port=8770

url_sending_port=url_receiving_port=9000
root_url_receiving_port=9100
#存放去重后的内链url
#url_queue=queue.Queue()
#unrepeated_url_queue=queue.Queue()
waiting_url=queue.Queue()
unrepeated_list_url=queue.Queue()
unrepeated_good_url=queue.Queue()
#去重容器
unrepeated_url_io=0
#set_=set()
url_set=set()
#信号量用于判断已接受到相应的ip与port
signal_ip=0
#存放内链标记
inner_href=".com"
##############接受####################
pr_=[]
po_=[]
start_signal=0
#################单独开一个守候线程#####################
def receiving_ip_and_port_from_server(spider_ip='127.0.0.1',ip_sending_port=8880,pr=0,po=0):
    global signal_ip,inner_href,pr_,po_,start_signal
    while(1):
        try:
            pr_,po_,inner_href=receiving_ip_and_port(spider_ip,ip_sending_port)
        except Exception as e:
            print("sth happended in receiving_ip_and_port():")
            print(e)
        #time.sleep(90)
        try:
            pr_,po_=check_proxy(pr_,po_)
        except Exception as e:
            print(e)
        #time.sleep(90)
        #print("\n----------可以联通的代理----------\n")
        while(pr_.qsize()>0):
            pr.put(pr_.get())
            po.put(po_.get())
        print("\n观察是否为空!!!!!")
        print("共接收到ip:%d个\n"%pr.qsize())
        start_signal=start_signal+1
        #time.sleep(4)
        signal_ip=0
    print('Receiving ip Server is running...')  
#一次性使用只是抽取首页的入口地址用用                                                                                                                                                                                                                                                                                                                                
def receiving_the_root_url_and_handling_them(spider_ip='127.0.0.1',url_sending_port=9000):
    global waiting_url,start_signal
    addr,data=TCP_receiver(spider_ip,url_sending_port)
    data=data.decode("utf-8")
    #print(data)
    for i in re.finditer("#.*?#",data):
        url=i.group()[1:-1]
        print("root_url: ",url)
        if(inner_href in url):  #防止非url因素的干扰
            de_weight(url_set,url)
            waiting_url.put(url) 
    start_signal=start_signal+1
    return True
##如果队列中ip数量较少,则发送请求并修改请求的信号量.
def if_the_ip_is_thirsty(remoted_ip='127.0.0.1',remoted_port=8880,pr=0,t_num=1):
    global signal_ip
    while(1):
        if(pr.qsize()<thread_num+1 and signal_ip==0):
            #lock.wait(t_num)
            print("sending the proxy ip ask..............")
            #time.sleep(4)
            try:
                TCP_sender(remoted_ip,remoted_port,"1111") #向主机请求资源
            except Exception as e:
                print(e)
            #lock.signal(t_num)
            signal_ip=1
            print("\n还有%d个可用ip"%pr.qsize())
##########接受url并填充至队列###########################
#from receiver import receiving_the_unrepeated_url
'''
TCP_sender('127.0.0.1',8880,"1111") #向主机请求资源以开启程序
receiving_ip_and_port(pr,po)             #开启一个守候线程单独给这个函数
'''
#receiving_the_root_url_and_handling_them("127.0.0.1",url_sending_port,url_queue)

#当信号量signal_ip为1时在等待接受ip，所以要想触发ip请求的话要if(signal_ip!=1 and pr.qsize()<thread_num+1)

################检查是否是商品页####################
def check_if_the_page_is_goods_pages(url):
    if("item" in url):
        r=requests.get(url)
        if("加入购物车" in r.text):
            return True
    return False
##################################################
def pick_up_price_class(text):
    #单纯提取数字会跟划去的价格混淆,计算机无法理解促销价以及售价,京东价是什么东西.....所以迫于无奈只能强行一对一匹配
    #淘宝,天猫还有的price有毒别人都是125.84它偏偏来个124.98-238.45
    #利用class中存在price/rmb的特点,一个一个class抽取匹配过去,然后根据相对位置定位具体位置
    global goods_name
    stack=queue.Queue()
    bs4=BeautifulSoup(text)
    for i in bs4.find(class_=re.compile('.*')):
        for j in re.findall('class\s*=\s*"[^"]+?"',str(i)):
            try:
                stack.put(re.search('".*"',str(j)).group()[1:-1])
            except Exception as e:
                print(e)
    times=0
    while(stack.qsize()>0):
        name=stack.get()
        if('price' in name or 'rmb' in name):
            #print('name: ',name)
            ########有两种模式要匹配:124.00 还有32.50-43.58 两种####
            for i in re.findall('>\s*\d+.\d+\s*<',str(bs4.find(class_=name))) or re.findall('>\s*\d+.\d+\s*-\s*\d+.\d+\s*<',str(bs4.find(class_=name))):
                times=times+1
                print('times:',times)
                #天猫还有163可以
                if(times==3):
                    price=str(i)[1:-1]
                    if(float(price)!=0):
                        print('name: ',name)
                        print('price: ',price)
                        return name
def pick_up_store_name(text):
    #进入店铺 或者 进店逛逛 的链接跟 店铺名的链接一样.....抓出来链接地址再匹配href中的链接地址就可以了
    #先把html编码进行一下转换
    text=text.replace('&gt;','>')
    text=text.replace('&lt;','<')
    bs4=BeautifulSoup(text)
    for i in bs4.find_all('a'):
        try:
            #print(i.get_text())
            if('进入店铺' in i.get_text() or '进店逛逛' in i.get_text()):
                #print(i)
                href=i.get('href')
                print(href)
                continue
        except Exception as e:
            print('店铺href定位bug: ',e)
    for i in bs4.find_all('a'):
        try:
            if(i.get('href') in href):
                #print(i.get_text())
                store_name=re.search('[^\s]+',i.get_text()).group()
                if(len(store_name)>4):
                    #店铺名一般都比较长。。。
                    return store_name
        except Exception as e:
            print('店铺匹配bug: ',e)
    return ''
goods_price_class=''
store_name=''
def pick_up_the_elements_and_store_in(driver):
    global goods_price_class
    text=driver.page_source
    try:
        goods_name=re.findall('[^-]+',driver.title)[0]
        if(goods_price_class==''):
            goods_price_class=pick_up_price_class(text)
        print('goods_price_class: ',goods_price_class)
        goods_price=re.search('>\s*\d+.\d+\s*<',str(bs4.find(class_=goods_price_class))).group()[1:-1]
        store_name=pick_up_store_name(text)
        print('title: ',goods_name)
        print('price: ',goods_price)
        print('store name: ',store_name)
    except Exception as e:
        print(e)
#高并发的商品信息收割机
def pick_up_elements():
    global unrepeated_good_url,pr,po
    while(pr.qsize()==0):
        print("ip 枯竭等待中....")
        time.sleep(1)
        pass
    proxy_ip=pr.get()  
    proxy_port=po.get()
    #使用的user-agent切换库
    driver=Driver(proxy_ip,proxy_port,40)
    while(1):
        while(unrepeated_good_url.qsize()>0):
            url=unrepeated_good_url.get()
            driver.get(url)
            pick_up_the_elements_and_store_in(driver)        
            
############爬虫的正式抓取阶段#######开启多线程(每个线程)进行抓爬###########
#分为 1.(可能)商品页面链接采集(利用/list这个通用标签) 注意别的网站添加http就可以,但是网易要添加http://you.163.com在前面
#     2.商品信息抓取利用//item(京东,天猫)或者/item(网易)或者//detail(天猫)
class_click_next_page=''
def pick_up_the_page_num(text):
    try:
        page_number=re.search("共.*[0-9]+.*页",text).group()
        page_number=int(re.search("[0-9]+",page_number).group())
    except Exception as e:
        if 'NoneType' in e:
            return 0
        print(e)
        page_number=1
        print('page_number: ',page_number)
    return page_number
def find_click_button(page):
    result=''
    #print("xxxxxxxx:")
    bs4=BeautifulSoup(page)
    for i in bs4.find_all('a'):
        if(re.search('下.*一.*页?',str(i))):
            result=str(i.get('class'))[2:-2]
    print('总页数: ',result)
    return result
def click_next_page():
    global driver,class_click_next_page
    if(class_click_next_page==''):
        try:
            class_click_next_page=find_click_button(driver.page_source)
        except Exception as e:
            print(e)
        print("wait!!!!!!!!!!!:",class_click_next_page)
        driver.find_element_by_class_name(class_click_next_page).click()
    else:
        print("wait!!!!!!!!!!!:",class_click_next_page)
        driver.find_element_by_class_name(class_click_next_page).click()
def pick_up_all_the_goods_url(page):
    bs4=BeautifulSoup(page)
    #print("yyyyyyyy:")
    for i in bs4.find_all("a"):
        #print(i.get('href'))
        try: 
            url=i.get('href')
            extend_the_url(url)
            if(inner_href in str(url)):    # error: argument of type 'NoneType' is not iterable
                if('//item' in str(url) and '#detail' not in str(url) and '163.com' not in str(url)):
                    if(de_weight(url_set,url)==0):
                        print('goods url -> http:'+str(url))
                if('/item' in str(url)):
                    if(de_weight(url_set,url)==0):
                        print('goods url -> http://you.163.com'+str(url))
                if('//detail' in str(url) and '163.com' not in str(url)):       #淘宝中的detail为重复链接需要去掉
                    if(de_weight(url_set,url)==0):
                        print('goods url -> http:'+str(url))
        except Exception as e:
            print('pick_up_all_goods_url:',e)  
#单独开一个线程抓取列表中的所有商品页面url
def pick_up_goods_url_in_lists():
    global inner_href,url_set,class_click_next_page,unrepeated_list_url,pr,po
    while(pr.qsize()==0):
        print("ip 枯竭等待中....")
        time.sleep(1)
        pass
    proxy_ip=pr.get()  
    proxy_port=po.get()
    #使用的user-agent切换库
    driver=Driver(proxy_ip,proxy_port,40)
    while(1):
        while(unrepeated_list_url.qsize()>0):
            url=unrepeated_list_url.get()
            print("开始采集list:%s里面的good_url"%url)
            time.sleep(1)
            driver.get(url)
            page_num=pick_up_the_page_num(driver.page_source)
            #print('5555555')
            bs4=BeautifulSoup(driver.page_source)
            #print('666666')
            pick_up_all_the_goods_url(driver.page_source)  #bug happen here
            #print('7777')
            for i in bs4.find_all('a'):
                try:
                    if(re.search('下.*一.*页?',str(i))):   
                        #print("get the page class:",class_click_next_page)
                        for j in range(page_num):
                            click_next_page()
                            print("\n开始翻页\n")
                            #bs4=BeautifulSoup(driver.page_source,'lxml')
                            pick_up_all_the_goods_url(driver.page_source)
                        break 
                except Exception as e:
                    print(e)
#单独开一个线程处理lists
def pick_up_all_lists_url():
    global inner_href,waiting_url,pr,po,start_signal,remoted_ip,url_checking_sending_port
    url=''
    while(pr.qsize()==0):
        print("ip 枯竭等待中....")
        time.sleep(1)
        pass
    proxy_ip=pr.get()  
    proxy_port=po.get()
    #使用的user-agent切换库
    driver=Driver(proxy_ip,proxy_port,40)
    while(start_signal<2):  #循环等待开启
        print("url_list is waiting for start.")
    #print("\n\nstart_signal:%d 为什么你不跑啊??\n\n"%start_signal)
    #time.sleep(4)
    while(waiting_url.qsize()>0):
        url=waiting_url.get()
        driver.get(url)
        bs4=BeautifulSoup(driver.page_source)
        for i in bs4.find_all('a'):
            try:
                url=i.get('href')
                if(inner_href in url):
                    extend_the_url(url)
                    if(de_weight(url_set,url)==0):
                        if('list' in url):
                            if(inner_href!='163.com' and '163.com' not in url or inner_href=='163.com'):
                                print("#pick up lists_url: ",url)
                                lock.wait(1)
                                TCP_sender(remoted_ip,url_checking_sending_port,url)
                                lock.signal(1)
                                #pick_up_all_lists_url(i.get('href'))
                        else:
                            lock.wait(1)
                            TCP_sender(remoted_ip,url_checking_sending_port,url)
                            lock.signal(1)
            except Exception as e:
                print("pick_up_all_lists_url: ",e)

def extend_the_url(url):
    if('http' not in url):
        url='http:'+url   

##########################接受验证了的url#################################
def processing_the_url(conn,data):
    global url_set
    global unrepeated_url_io
    if data in url_set:
        q=data+"  repeated"
        #lock.wait(t_num)
        try:
            conn.sendall(q.encode())
        except Exception as e:
            print(e)
        #lock.signal(t_num)
    else:
        if('http' not in data):
            data='http:'+data
        q=data+"  unrepeated"
        print("接受到的正确的url: ",q)
        #print('\n吞吐量计数: ',unrepeated_url_io,'\n')
        #lock.wait(t_num)
        url_set.add(str(data))
        try:
            conn.sendall(q.encode())
        except Exception as e:
            print(e)
class MyServer(socketserver.BaseRequestHandler):
    def handle(self):
        global unrepeated_good_url,unrepeated_list_url,waiting_url
        conn = self.request
        while (1):
            lock.acquire()
            print("start listening.")
            try:
                data = conn.recv(1024)
            except Exception as e:
                print(e)
                continue
            lock.release()
            cur_thread = threading.current_thread()
            if(data!=b"" and "unrepeated" in data):
                data=str(data)[1:]
                print(cur_thread.name,"  :",data)    
                if("unrepeated" in data):
                    if('list' in data):
                        print("-->unrepeated list_url: ",data)
                        unrepeated_list_url.put(data)
                    elif('/item' in data):
                        print("-->unrepeated good_url: ",data)
                        unrepeated_good_url.put(data)
                    elif('//detail' in data):       #淘宝中的detail为重复链接需要去掉
                        print("-->unrepeated good_url: ",data)
                        unrepeated_good_url.put(data)
                    else:
                        print("-->other types url:",data)
                        waiting_url.put(data)
                time.sleep(1)
            #time.sleep(2)

def receiving_the_unrepeated_url(remoted_ip='127.0.0.1',url_checking_port=8770):
    global unrepeated_url_queue,unrepeated_list_url,unrepeated_good_url
    server = socketserver.ThreadingTCPServer((remoted_ip,url_checking_port),MyServer)
    ip,port=server.server_address
    server.serve_forever()               
#单独开一个线程              
#receiving_the_unrepeated_url
##################主函数########################

if __name__ == "__main__":  
    '''
http://top.jd.com/?itemId=1424211542&cateId=12358 into the set
---No.1 申请资源成功....
No.1 释放锁......
Adding this url http://top.jd.com/?itemId=1424211542&cateId=654 into the set
---No.1 申请资源成功....
No.1 释放锁......
Adding this url http://top.jd.com/?itemId=1424211542&cateId=5012 into the set
---No.1 申请资源成功....
No.1 释放锁......
Adding this url http://top.jd.com/?itemId=1424211542&cateId=844 into the set
---No.1 申请资源成功....
No.1 释放锁......
Adding this url http://top.jd.com/?itemId=1424211542&cateId=831 into the set
---No.1 申请资源成功....
No.1 释放锁......
Adding this url http://top.jd.com/?itemId=1424211542&cateId




    1.先挂起一个ip检测线程。
    if_the_ip_is_thirsty(remoted_ip='127.0.0.1',remoted_port=8880,pr=0,t_num=1)
    2.再挂起一个ip接受进程
    receiving_ip_and_port(spider_ip='127.0.0.1',ip_sending_port=8880,pr=0,po=0)
    3.然后再挂起一个url接受进程
    
    receiving_the_unrepeated_url(remoted_ip,url_checking_port,url_queue)
    
    开始run 1.2.3
    
    4.获取网页入口地址
    receiving_the_root_url_and_handling_them(spider_ip='127.0.0.1',url_sending_port=9000,url_queue=0)
    5.开启一个进程运行scraping函数（发送验证信息）
    ------------
    6.从队列中取出经加工后的url,抓取数据
    
    question1.怎么在商品页上翻页并且不会翻页翻到最后卡死? 
    question2.
    '''
    threads=[]
    #请求ip同时开启ip池监视模块
    t=threading.Thread(target=if_the_ip_is_thirsty,args=(remoted_ip,ip_sending_port,pr,1,))#t_num=1
    threads.append(t)
    #开启ip接受模块
    t=threading.Thread(target=receiving_ip_and_port_from_server,args=(spider_ip,ip_receiving_port,pr,po,))
    threads.append(t)
    #提前开启url接受模块
    t=threading.Thread(target=receiving_the_unrepeated_url,args=(spider_ip,url_checking_receiving_port,))
    threads.append(t)

    #先把goods url爬成功再来搞信息提取
    
    for i in range(thread_num):
        t=threading.Thread(target=pick_up_elements,args=())
        threads.append(t)

    for i in range(len(threads)):
        print("start第%d个线程"%i)
        threads[i].start() 
    #接收root url
    print("xxxxxxxxxxxxxxxxxxxxxxxxxxxxx:")
    receiving_the_root_url_and_handling_them(spider_ip,root_url_receiving_port)    
    print("跑通啦!!!!!!!!!!!!!!!!!!!!!!!")
    #开始爬取并且边爬边发送去重请求。
    t=threading.Thread(target=pick_up_all_lists_url,args=())#t_num=1
    threads.append(t)
    t.start()
    for i in range(len(threads)):
        threads[i].join()    
    #receiving_the_root_url_and_handling_them(spider_ip=spider_ip,url_sending_port=url_sending_port,url_queue=url_queue) 
